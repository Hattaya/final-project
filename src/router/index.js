import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Homepage.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Homepage',
    component: Home
  },
  {
    path: '/Home',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/manageStaff',
    name: 'ManageStaff',
    component: () => import('../views/ManageStaff.vue')
  },
  {
    path: '/manageDevices',
    name: 'ManageDevices',
    component: () => import('../views/ManageDevices.vue')
  },
  {
    path: '/manageStatus',
    name: 'ManageStatus',
    component: () => import('../views/ManageStatus.vue')
  },
  {
    path: '/manageBuilding',
    name: 'ManageBuilding',
    component: () => import('../views/ManageBuilding.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login.vue')
  },
  {
    path: '/notification',
    name: 'notification',
    component: () => import('../views/Staff.vue')
  },
  {
    path: '/repairHistory',
    name: 'repairHistory',
    component: () => import('../views/StaffHistory.vue')
  },
  {
    path: '/history',
    name: 'history',
    component: () => import('../views/InformerHistory.vue')
  },
  {
    path: '/repair',
    name: 'repair',
    component: () => import('../views/Informer.vue')
  },
  {
    path: '/repairNotification',
    name: 'repairNotification',
    component: () => import('../views/InformerRepairNoti.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
