import axios from 'axios'
const url = 'http://localhost:3000/statuses'
const statusService = {
  addNew_to_DB (status) {
    return axios.post(url, status)
  },
  updateStatus_to_DB (status) {
    return axios.put(url, status)
  },
  changeState_to_DB (status) {
    return axios.put(url, status)
  },
  getAll_from_DB () {
    return axios.get(url)
  }
}
export default statusService
