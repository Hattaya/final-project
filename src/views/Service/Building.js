import axios from 'axios'
const url = 'http://localhost:3000/building'
const buldService = {
  addNew_to_DB (build) {
    return axios.post(url, build)
  },
  updateBuild_to_DB (build) {
    return axios.put(url, build)
  },
  changeState_to_DB (build) {
    return axios.put(url, build)
  },
  getAll_from_DB () {
    return axios.get(url)
  }
}
export default buldService
