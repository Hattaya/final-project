import axios from 'axios'
const url = 'http://localhost:3000/repairNotofication'
const repairService = {
  addNew_to_DB (repair) {
    return axios.post(url, repair)
  },
  updaterepair_to_DB (repair) {
    return axios.put(url, repair)
  },
  changeState_to_DB (repair) {
    return axios.put(url, repair)
  },
  getAll_from_DB () {
    return axios.get(url)
  }
}
export default repairService
