import axios from 'axios'
const url = 'http://localhost:3000/devices'
const devicesService = {
  addNew_to_DB (device) {
    return axios.post(url, device)
  },
  updatedevice_to_DB (device) {
    return axios.put(url, device)
  },
  changeState_to_DB (device) {
    return axios.put(url, device)
  },
  getAll_from_DB () {
    return axios.get(url)
  }
}
export default devicesService
