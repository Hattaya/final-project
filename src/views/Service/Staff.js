import axios from 'axios'
const url = 'http://localhost:3000/staffs'
const staffService = {
  addNew_to_DB (staff) {
    return axios.post(url, staff)
  },
  updateStaff_to_DB (staff) {
    return axios.put(url, staff)
  },
  changeState_to_DB (staff) {
    return axios.put(url, staff)
  },
  getAll_from_DB () {
    return axios.get(url)
  }
}
export default staffService
