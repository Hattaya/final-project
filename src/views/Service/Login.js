import axios from 'axios'
const loginService = {
  async login (model) {
    return await axios
      .post('http://localhost:3000/staffs/login', { ...model })
      .then(res => {
        console.log(res)
        if (res && res.data) {
          if (res.data.usability === 'cancel') {
            alert('username หรือ password ไม่ถูกต้อง')
            return { result: false }
          }
          localStorage.setItem('account', JSON.stringify(res.data))
          return { result: true, user: res.data }
        } else {
          alert('username หรือ password ไม่ถูกต้อง')
        }
        return { result: false }
      })
      .catch(err => {
        console.log(err)
      })
  },
  logout () {
    localStorage.clear()
    window.location.href = '/'
  }
}
export default loginService
